# Jmeter Scenario for Git Automation

Jmeter Scenario/Script for Git Automation

ReadME:
1. Scenario consists of transactions with thinktime,response assertions and settings required for load test. 
    Load Test Scenario:
    Users - 100
    Ramp UP - 1 user 1 sec
    Steady State - 1 HR
    Ramp DOWN - 1 user 1 sec
    TOTAL DURATION - 1 Hr 10 Min
2. It has influxdb backend listener which will write the real time test data and can be visible on grafana dashboard.Demo is attached.
3. BeanShell Timer is configured which will write the current state of MR along with MR ID and Branch Name to csv file for tracking purpose.
4. CI-CD Pipeline is also added with scenario for very less duration.

